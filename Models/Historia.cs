﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace dalesi.Models
{
    public class Historia
    {
       [Key]
       public int id { get; set; }
       
       public string nombre { get; set; }

       public string info { get; set; }

    }
}
